package com.example.angularCrud.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileService {
	
	final static String FILE_DIRECTORY = "file.repository.path";
	final static String fileSeparator = File.separator;

	@Autowired
	private Environment environment;
	
	public void storeFiledgdf(MultipartFile file) throws IOException {
		Path filePath = Paths.get(environment.getProperty(FILE_DIRECTORY) + fileSeparator + file.getOriginalFilename());
 
		Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
	}
	
	public boolean storeFile(MultipartFile file) throws IOException {
		try {
			String fileName=generateDocumentName()+file.getOriginalFilename();
			System.out.println("saveAttachment : " + environment.getProperty(FILE_DIRECTORY) + fileSeparator + fileName);
			
			File oldFile =new File(environment.getProperty(FILE_DIRECTORY) + fileSeparator + fileName);
			System.out.println("oldFile.exists() :: "+oldFile.exists());
			File destination = new File(environment.getProperty(FILE_DIRECTORY).concat(fileName));

//			if(oldFile.exists()) {
//				Files.copy(file.getInputStream(), Paths.get(environment.getProperty(FILE_DIRECTORY) + fileSeparator + fileName), StandardCopyOption.REPLACE_EXISTING);
//			} else {
				Files.createDirectories(Paths.get(destination.getAbsolutePath()).getParent());
				Files.copy(file.getInputStream(), Paths.get(environment.getProperty(FILE_DIRECTORY) + fileSeparator + fileName), StandardCopyOption.REPLACE_EXISTING);
			//}
			file.getInputStream().close();
			return true;
		} catch (FileAlreadyExistsException fe) {
			System.err.println("Exception : File Already Exists so printStackTrace not printed.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public String generateDocumentName() 
	{
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyMMddhhmmssMsn"));
	}
	
}
