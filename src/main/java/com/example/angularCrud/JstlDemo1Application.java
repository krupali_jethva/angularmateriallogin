package com.example.angularCrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JstlDemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(JstlDemo1Application.class, args);
	}

}
