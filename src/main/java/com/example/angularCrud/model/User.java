package com.example.angularCrud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema="public",name = "user_master")
public class User {
	
	@Id 
	@SequenceGenerator(name="user_master_id_seq", sequenceName="user_master_id_seq",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_master_id_seq")
	@Column(name="id", unique=true, nullable=false)
	private Long id;

	@Column(name="user_name")
	private String username;
	
	@Column(name="email")
    private String email;
    
	@Column(name="user_pwd")
    private String password;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
}
