package com.example.angularCrud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.angularCrud.model.User;
import com.example.angularCrud.repository.UserRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class UserController {

	@Autowired
	UserRepository repository;
	
	@PostMapping(value="/addUser")
	public ResponseEntity<User> saveUser(@RequestBody User user)
	{
		System.out.println("In user");
		try{
			User _user=repository.save(user);
			return new ResponseEntity<>(_user,HttpStatus.CREATED);
		}catch (Exception e) {
			return new ResponseEntity<>(null,HttpStatus.EXPECTATION_FAILED);	// TODO: handle exception
		}		
	}
	
	@GetMapping(value="/users")
	public ResponseEntity<List<User>> getAllUser()
	{
		List<User> userList=new ArrayList<>();
		
		System.out.println("In users");
		try{
		    
			repository.findAll().forEach(userList::add);
			
			if(userList.isEmpty())
			{
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}			
			return new ResponseEntity<>(userList,HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);	// TODO: handle exception
		}		
	}
}
