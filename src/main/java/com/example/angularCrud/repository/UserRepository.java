package com.example.angularCrud.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.angularCrud.model.User;


@Repository 
public interface UserRepository extends CrudRepository<User, Long>{

}
